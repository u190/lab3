import java.io.IOException;
import java.util.Random;
import java.util.Scanner;


public class GuessNumber {

    public static void main(String[] args)throws IOException {
        Scanner reader = new Scanner(System.in);
        Random rand = new Random();
        int number =rand.nextInt(100);


        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");

        int guess = reader.nextInt();
        if (number == guess){
            System.out.println("Congratulations! \n");

        }else{
            System.out.println("Sorry, the number was "+ number);
        }


    }
}


