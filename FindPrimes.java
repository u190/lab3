import java.util.Scanner;
public class FindPrimes {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a positive integer you want.:");
        int number = reader.nextInt();
        int control=2;
        while(control < number){
            boolean primeNumber = true;
            for(int i =2 ; i<control ; i++){
                if(control % i == 0){
                    primeNumber = false;
                }
            }
            if (primeNumber) {
                System.out.print(control + ",");
            }
            control++;
        }
    }
}

